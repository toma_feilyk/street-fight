function getDiscount(number) {
  var sum = 100;
  var result;

  if (number < 5) {
    result = sum;
  } else if (number >= 5 && number < 10) {
    result = sum * 0.95;
  } else {
    result = sum * 0.9;
  }
  return result;
}

console.log(`The price is ${getDiscount(100)}`);
