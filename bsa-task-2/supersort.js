const text = 'mic09ha1el 4b5en6 michelle be4atr3ice';

function superSort(text) {
  var array = text.split(' ');

  console.log(array.sort(sortFunction));
}

function deleteNumbers(text) {
  return text.replace(/[0-9]/g, '');
}

function sortFunction(text1, text2) {
  const filteredText1 = deleteNumbers(text1);
  const filteredText2 = deleteNumbers(text2);
  if (filteredText1 < filteredText2) {
    return -1;
  } else if (filteredText1 > filteredText2) {
    return 1;
  } else {
    return 0;
  }
}

superSort(text);
