setTimeout(function () {
  const bubbles = document.querySelectorAll('.bubble');
  const bubblesCount = bubbles.length;
  console.log(`Bubbles count: ${bubblesCount}`);
  bubbles.forEach((bubble) => {
    bubbleClick(bubble);
  });
  expect(bubblesCount);
}, 5000);

function bubbleClick(bubble) {
  bubble.onclick();
}

function expect(bubblesCount) {
  const score = parseInt(document.querySelector('#score').innerText);
  if (bubblesCount === score) {
    console.log('Expected score is equal to actual score');
  } else {
    console.log('Expected score is not equal to actual score');
  }
}

//EXTRA
// var i = 0;

// function infinityClick() {
//   const bubbles = document.querySelectorAll('.bubble');

//   bubbles.forEach((bubble) => {
//     bubbleClick(bubble);
//     i++;
//     const score = document.querySelector('#score').innerText;
//     if (score == i) {
//       console.log('Score increased');
//     }
//   });
//   window.requestAnimationFrame(infinityClick);
// }

// window.requestAnimationFrame(infinityClick);
