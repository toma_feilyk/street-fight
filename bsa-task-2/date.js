function areDatesEqual(date1, date2) {
  const dateObj1 = formatDate(date1);
  const dateObj2 = formatDate(date2);

  return (
    dateObj1.day == dateObj2.day &&
    dateObj1.month == dateObj2.month &&
    dateObj1.year == dateObj2.year
  );
}

function formatDate(date) {
  const separator = getSeparator(date);
  const obj = date.split(separator);

  var day, month, year;
  if (obj[0].length == 4) {
    year = obj[0];

    if (obj[1] > '12') {
      day = obj[1];
      month = obj[2];
    } else if (obj[2] > '12') {
      day = obj[2];
      month = obj[1];
    } else {
      day = obj[1];
      month = obj[2];
    }
  } else {
    year = obj[2];

    if (obj[0] > '12') {
      day = obj[0];
      month = obj[1];
    } else if (obj[1] > '12') {
      day = obj[1];
      month = obj[0];
    } else {
      day = obj[0];
      month = obj[1];
    }
  }

  return {
    day,
    month,
    year,
  };
}

function getSeparator(date) {
  if (date.includes(',')) {
    return ',';
  } else if (date.includes('.')) {
    return '.';
  } else if (date.includes('/')) {
    return '/';
  } else if (date.includes('-')) {
    return '-';
  }
}

console.log(areDatesEqual('01,11,1999', '05/12/2012'));
console.log(areDatesEqual('21.02.1999', '1999-21-02'));
